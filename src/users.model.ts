export enum UserState {

}

export enum UserStatus {

}

export interface IUser {
    activated_at: Date,
    created_at: Date,
    updated_at: Date,
    directory_id: null | number,
    distinguished_name: null | string,
    email: string,
    external_id: null | any,
    firstname: string,
    lastname: string,
    group_id: null | number,
    id: number,
    invalid_login_attempts: number,
    invitation_sent_at: null | Date,
    last_login: Date,
    locked_until: null | Date,
    member_of: null | any,
    password_changed_at: null | Date,
    phone: string,
    samaccountname: null | string,
    state: UserState,
    status: UserStatus,
    username: string,
    custom_attributes: {
        [key: string]: any
    }
}
