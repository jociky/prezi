import axios from "axios";

export class UserService {
    private _endpoint = 'api/2/users';

    getUsers() {
        return axios.get(this._endpoint);
    }

    getUser(id: number) {
        return axios.get(this._endpoint + '/' + id.toString());
    }

    updateUser(id: number, data: any) {
        return axios.put(this._endpoint + '/' + id.toString(), data);
    }
}