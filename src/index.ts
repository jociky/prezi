import axios from "axios"
import YAML from 'yaml'
import commander from 'commander';

import { UserService } from "./user.service";
import { IUser } from "./users.model";

import * as fs from 'fs';
import * as path from 'path';



const program = new commander.Command();

program
    .option('-b, --baseUrl [baseUrl]', 'Base URL to listen on', 'http://localhost:3337')
    .option('-t, --token [token]', 'Authorization token to use for HTTP requests', 'SG9sYSBEYW5pZWxhISBDw7NtbyBlc3TDoXMgaG95Pwo=')
    .option('-o, --output [output]', 'Path where the output should be written', 'users.yml')
    .parse(process.argv)
const opts = program.opts();

axios.defaults.baseURL = opts.baseUrl;
axios.defaults.headers.common.authorization = 'bearer ' + opts.token;

const outDir = path.dirname(opts.output)
if(!fs.existsSync(outDir)) {
    fs.mkdirSync(outDir, { recursive: true });
}

async function main() {
    const userSvc = new UserService();
    const users: IUser[] = (await userSvc.getUsers()).data;

    const outFile = fs.createWriteStream(opts.output);

    users.map((user: IUser) => {
        return {
            apiVersion: 'backstage.io/v1alpha1',
            kind: 'User',
            metadata: {
                name: user.username
            },
            spec: {
                memberOf: ['guests']
            }
        }
    }).forEach((user) => {
        outFile.write('---\n' + YAML.stringify(user));
    });

}

main();